﻿using Foundation;
using Riddha.CourierMS.DependencyServices;
using Riddha.CourierMS.iOS.DependencyServices;
using System;
using UIKit;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(Toast), typeof(Toast_IOS))]
namespace Riddha.CourierMS.iOS.DependencyServices
{
    public class Toast_IOS : Toast
    {
        const double LONG_DELAY = 3.5;


        NSTimer alertDelay;
        UIAlertController alert;

        public void Show(string message)
        {
            ShowAlert(message, LONG_DELAY);
        }


        void ShowAlert(string message, double seconds)
        {
            alertDelay = NSTimer.CreateScheduledTimer(seconds, (obj) =>
            {
                dismissMessage();
            });
            alert = UIAlertController.Create(null, message, UIAlertControllerStyle.Alert);
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(alert, true, null);
        }
        void dismissMessage()
        {
            if (alert != null)
            {
                alert.DismissViewController(true, null);
            }
            if (alertDelay != null)
            {
                alertDelay.Dispose();
            }
        }

    }
}
