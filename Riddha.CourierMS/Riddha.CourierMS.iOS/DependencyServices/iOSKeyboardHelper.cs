﻿using Foundation;
using Riddha.CourierMS.DependencyServices;
using Riddha.CourierMS.iOS.DependencyServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(IKeyboardHelper), typeof(iOSKeyboardHelper))]
namespace Riddha.CourierMS.iOS.DependencyServices
{
   public class iOSKeyboardHelper : IKeyboardHelper
    {
        public void HideKeyboard()
        {
            UIApplication.SharedApplication.KeyWindow.EndEditing(true);
        }

        public void ShowKeyboard()
        {
            UIApplication.SharedApplication.KeyWindow.EndEditing(false);
        }
    }
}